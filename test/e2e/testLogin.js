require('../../nightwatch.conf.js');

var qaLogin;

module.exports = {
  beforeEach: function (browser) {
    qaLogin = browser.page.qaLogin();
    browser.windowMaximize();
  },
  'Login Test Form - Username, Customer and Password -> Wrong': function (browser) {
    qaLogin.standardLogin(browser, 'InvalidUser', 'InvalidCustomer', 'InvalidPassword123!', false)
  },
  'Login Test Form - Username -> Wrong': function (browser) {
    qaLogin.standardLogin(browser, 'InvalidUser', 'qa', 'Carlitros1242,', false)
  },
  'Login Test Form - Customer -> Wrong': function (browser) {
    qaLogin.standardLogin(browser, 'CarlosVicente2', 'InvalidCustomer', 'Carlitros1242,', false)
  },
  'Login Test Form - Password -> Wrong': function (browser) {
    qaLogin.standardLogin(browser, 'CarlosVicente2', 'qa', 'InvalidPassword123!', false)
  },
  'Login - Valid Username': function (browser) {
    qaLogin.standardLogin(browser, 'CarlosVicente2', 'qa', 'Carlitros1242,', true)
  },
  'Login - Valid Username with LDAP': function (browser) {
    qaLogin.ldapLogin(browser, 'CarlosVicente@bf.local ', 'Carlitros1242,', true)
  },
  "Login - Invalid Username": function (browser) {
    qaLogin.standardLogin(browser, 'UserFail', 'qa', 'Passw0rd!', false);
  },
  'Login - Invalid Username with LDAP': function (browser) {
    qaLogin.ldapLogin(browser, 'invalidUser@bf.local ', 'Passw0rd!', false)
  },
  "Login - User expired": function (browser) {
    qaLogin.standardLogin(browser, 'expiredUser', 'qa', 'Carlitros1242,', false);
  },
  "Login - Expired password": function (browser) {
    qaLogin.standardLogin(browser, 'expiredPassword', 'qa', 'Carlitros1242,', false);
  },
  "Login - Request new password - Valid Username": function (browser) {
    qaLogin.requestPassword(browser, 'CarlosTesting', 'qa', true);
  },
  "Login - Request new password - Invalid Username": function (browser) {
    qaLogin.requestPassword(browser, 'InvalidUsername', 'qa', false);
  },
  "Login - Request new password - Invalid Customer": function (browser) {
    qaLogin.requestPassword(browser, 'CarlosTesting', 'InvalidCustomer', false);
  },
  "Login - Request new password - Invalid Username & Customer": function (browser) {
    qaLogin.requestPassword(browser, 'InvalidUsername', 'InvalidCustomer', false);
  }
};