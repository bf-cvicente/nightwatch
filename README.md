# nightwatch

# Installation:
npm install nightwatch

# Start all tests:
nightwatch

# Start all tests of one directory:
nightwatch test/e2e

# Run only 1 test:
nightwatch test/e2e/testName.js

# Execute tests by 1 tag:
nightwatch --tag nameTag

# Execute tests by more than 1 tag:
nightwatch --tag nameTag1 --tag nameTag2

# Test with different browsers in parallel:
nightwatch -e default,chrome,firefox