const loginCommands = {
  //Standard login
  standardLogin: function (browser, username, customer, password, expectedResult) {
    this.navigate()
      .waitForElementVisible('body')
      .waitForElementVisible('@loginButton', 1000)
      .setValue('@username', username)
      .setValue('@customer', customer)
      .setValue('@password', password)
      .submitForm('@loginButton');

    expectedResult ? this.waitForElementPresent('#navbar-masterdata') : this.waitForElementNotPresent('#navbar-masterdata', 5000, false, function() {},
     'The user has managed to make an action that should not have been possible.');

    browser.url('https://qa.bigengine.io/logout')
      .end()
      .closeWindow()
  },
  //LDAP login
  ldapLogin: function (browser, username, password, expectedResult) {
    this.navigate()
      .waitForElementVisible('body')
      .waitForElementVisible('@loginButton', 1000)
      .click('@ldapButton')
      .api.pause(4000);

    browser.window_handles(function (result) {
      var handle = result.value[1];
      browser.switchWindow(handle);
    });

    this.setValue('@usernameLDAP', username)
      .setValue('@passwordLDAP', password)
      .submitForm('@ldapLoginButton')

    if (expectedResult) {
      this.waitForElementVisible('@closeLDAPWindow')
        .click('@closeLDAPWindow')
      browser.window_handles(function (result) {
        var handle = result.value[0];
        browser.switchWindow(handle);
      });
      this.waitForElementPresent('#navbar-masterdata');
      browser.url('https://qa.bigengine.io/logout')
        .end()
        .closeWindow()
    } else {
      this.waitForElementNotPresent('#navbar-masterdata', 4000);
    }
  },
  //Request new password
  requestPassword: function (browser, username, customer, expectedResult) {
    this.navigate('https://qa.bigengine.io/forgot')
      .setValue('@usernameForgot', username)
      .setValue('@customerForgot', customer)
      .click('@submitForgot')

      expectedResult ? this.waitForElementNotPresent('.toast-error') : this.waitForElementPresent('.toast-error', 5000, false, function() {},
       'The user has managed to make an action that should not have been possible.');

      browser.url('https://qa.bigengine.io/logout')
      .end()
      .closeWindow()
  }
};

module.exports = {
  commands: [loginCommands],
  url: function () {
    return 'https://qa.bigengine.io/';
  },
  elements: {
    //Login Form
    username: '#loginForm > div:nth-child(3) > input',
    customer: '#loginForm > div:nth-child(4) > input',
    password: '#loginForm > div:nth-child(5) > input',
    loginButton: '#loginForm > button',
    ldapButton: '#loginForm > a.btn.btn-primary.block.full-width.m-b',
    //LDAP Form
    usernameLDAP: '#userNameInput',
    passwordLDAP: '#passwordInput',
    ldapLoginButton: '#submitButton',
    closeLDAPWindow: '#wrapper > div > div > div > div > a',
    //Forgot Password Form
    usernameForgot: '#signupForm > div:nth-child(2) > input',
    customerForgot: '#signupForm > div:nth-child(3) > input',
    submitForgot: '#signupForm > button'
  }
};